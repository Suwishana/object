package dp.creational.singelton;

public class SingletonObj {
	private static SingletonObj singletonObj;
private SingletonObj() {
    }
public static SingletonObj getInstance() {
	if (singletonObj== null) {
		singletonObj=new SingletonObj();
	}
	return singletonObj;
	
}
public static void setNull() {
	singletonObj=null;


}
}
