package dp.creational.singelton;

public class SingletonObjDemo {
	public static void main (String[]args) {
		//SingletonObj = new SingletonObj();
		SingletonObj obj =SingletonObj.getInstance();
		System.out.println(obj);
		obj.setNull();
		SingletonObj obj2 =SingletonObj.getInstance();
		System.out.println(obj2);
		
		
	}

}
//dp.creational.singelton.SingletonObj@26f0a63f
//dp.creational.singelton.SingletonObj@4926097b


